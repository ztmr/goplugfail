PROJNAME:=goplugfail
GOVSN?=1.12.9

all: loader pluginmgr.so demoplug1.so

.PHONY: docker-image docker-test

docker-image: Dockerfile
	@docker build --build-arg GOVSN=$(GOVSN) -t $(PROJNAME) .

docker-test: docker-image
	@docker run -ti --rm $(PROJNAME)

loader: loader.c
	gcc -o $@ $< -ldl -rdynamic

demoplug1.so: demoplug1.go
	go build -o $@ -buildmode=plugin $<

pluginmgr.so: pluginmgr.go
	go build -o $@ -buildmode=c-shared $<

test:
	@echo "Running pluginmgr directly..."
	@go run pluginmgr.go
	@echo "Running pluginmgr as a shared library invoked from C wrapper..."
	@./loader ./pluginmgr.so ./demoplug1.so

clean:
	@rm -f loader *.so *.h

