package main

import "fmt"

type PluginData struct{}

func (_ PluginData) Init() {
	fmt.Println("Hello from Plugin.Init!")
}

var Plugin PluginData
