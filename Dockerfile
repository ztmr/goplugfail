ARG GOVSN=1.12.9
FROM golang:${GOVSN}
COPY ./ /go/
WORKDIR /go
RUN make all test || true \
 && echo "=== Repeat the same tests with GODEBUG=cgocheck=2" \
 && env GODEBUG="cgocheck=2" make test
