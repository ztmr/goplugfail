package main

import "C"
import (
	"fmt"
	"os"
	"plugin"
)

type Greeter interface {
	Greet()
}

func main() {
	LoadPluginInternal("demoplug1.so")
}

//export LoadPlugin
func LoadPlugin(name *C.char) {
	goName := C.GoString(name)
	LoadPluginInternal(goName)
}

type Plugin interface {
	Init()
}

func LoadPluginInternal(mod string) {
	fmt.Printf("Loading plugin: >>>%v<<<\n", mod)
	plug, err := plugin.Open(mod)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	symPlugin, err := plug.Lookup("Plugin")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	var plugin Plugin
	plugin, ok := symPlugin.(Plugin)
	if !ok {
		fmt.Println("unexpected type from module symbol")
		os.Exit(1)
	}
	plugin.Init()
}
