#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>

int main(int argc, char * argv []) {
    void *handle;
    void (*func_load_plugin)(char*);

    if (argc != 3) {
      fprintf(stderr, "Usage: %s <pluginMgrLib> <goPluginLib>\n", argv[0]);
      return EXIT_FAILURE;
    }

    handle = dlopen(argv[1], RTLD_LAZY);
    if (!handle) {
        /* fail to load the library */
        fprintf(stderr, "Error: %s\n", dlerror());
        return EXIT_FAILURE;
    }

    *(void**)(&func_load_plugin) = dlsym(handle, "LoadPlugin");
    if (!func_load_plugin) {
        /* no such symbol */
        fprintf(stderr, "Error: %s\n", dlerror());
        dlclose(handle);
        return EXIT_FAILURE;
    }

    func_load_plugin(argv[2]);
    dlclose(handle);

    return EXIT_SUCCESS;
}
